load data
   infile 	'centre.txt'
   badfile 	'centre.bad'
   discardfile 	'centre.dsc'
INSERT 
into table CENTRETRAITEMENT
fields terminated by ';' 
trailing nullcols ( NOCENTRE   "seq_centre.nextval",
		    NOM,
		    NORUE,
		    RUE,
		    CPOSTAL,
		    VILLE )

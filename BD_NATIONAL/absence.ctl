load data
   infile 	'absence.txt'
   badfile 	'absence.bad'
   discardfile 	'absence.dsc'
INSERT 
into table ABSENCE
fields terminated by ';' 
trailing nullcols ( NOABSENCE "seq_absence.nextval",
		    DATEDEBUT,
		    DATEFIN,
		    REMARQUE,
                    NOTYPEABSENCE,
		    NOEMPLOYE )

